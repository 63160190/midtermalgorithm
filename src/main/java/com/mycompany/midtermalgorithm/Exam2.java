/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.MidtermAlgorithm;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Exam2 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        long startTime = System.nanoTime();
        int n = kb.nextInt();
        int[] arr = new int[n];

        InputlenArray(n, arr, kb);
        //lenSubArray(n, arr);

        System.out.print(lenSubArray(n, arr));
        time(startTime);
    }

    public static void time(long startTime) {
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;

        double seconds = (double) totalTime / 1000000;
        System.out.println("\nTotalTime a program " + seconds);
    }

    public static void InputlenArray(int n, int[] arr, Scanner kb) {
        for (int i = 0; i < n; i++) {
            arr[i] = kb.nextInt();
        }
    }

    public static int lenSubArray(int n, int[] arr) {
        int max = 1, len = 1, maxIndex = 0;
        for (int i = 1; i < n; i++) {
            if (arr[i] > arr[i - 1]) {
                len++;
            } else {
                if (max < len) {
                    max = len;
                }
                len = 1;
            }
        }
        if (max < len) {
            max = len;
        }
        return max;
    }

}
