/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.MidtermAlgorithm;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Exam1 {

    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);
        long startTime = System.nanoTime();
        int c = kb.nextInt();
        int[] arr = new int[c];

        inputArray(c, arr, kb);
        reversal(c, arr);
        ShowArray(c, arr);

        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;

        double seconds = (double) totalTime / 1000000;
        System.out.println("\nTotalTime a program "+seconds);
    }

    public static void ShowArray(int c, int[] arr) {
        for (int i = 0; i < c; i++) {
            System.out.print(arr[i]+" ");
            
        }
    }

    public static void reversal(int c, int[] arr) {
        for (int i = 0; i < c / 2; i++) {
            int j = arr[i];
            arr[i] = arr[c - i - 1];
            arr[c - i - 1] = j;
        }
    }

    public static void inputArray(int c, int[] arr, Scanner kb) {
        for (int i = 0; i < c; i++) {
            arr[i] = kb.nextInt();
        }
    }
}
